'use strict';

/**
 * @ngdoc overview
 * @name ebookerApp
 * @description
 * # ebookerApp
 *
 * Main module of the application.
 */
angular
  .module('ebookerApp', [
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'indexedDB'
  ])

  .config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(true);
  }])

  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
  }])

  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'ListCtrl',
        controllerAs: 'list'
      })
      .when('/list', {
        templateUrl: 'views/list/view.html',
        controller: 'ListCtrl',
        controllerAs: 'list',
        method: 'listView'
      })
      .when('/list/:id', {
        templateUrl: 'views/list/view.html',
        controller: 'ListCtrl',
        controllerAs: 'list',
        method: 'listView'
      })
      .when('/list/delete', {
        templateUrl: 'views/list/delete.html',
        controller: 'ListCtrl',
        controllerAs: 'list',
        method: 'listDelete'
      })
      .when('/list/delete/:id', {
        templateUrl: 'views/list/delete.html',
        controller: 'ListCtrl',
        controllerAs: 'list',
        method: 'listDelete'
      })
      .when('/list/delete/:id/confirm', {
        templateUrl: 'views/list/delete.html',
        controller: 'ListCtrl',
        controllerAs: 'list',
        method: 'listDeleteConfirm'
      })
      .when('/book/:id', {
        templateUrl: 'views/book/view.html',
        controller: 'BookCtrl',
        controllerAs: 'book',
        method: 'bookView'
      })
      .otherwise({
        redirectTo: '/'
      });
  }])

  .config(['$indexedDBProvider', function ($indexedDBProvider) {
    $indexedDBProvider
      .connection('ebookerDB')
      .upgradeDatabase(1, function (event, db) {
        var objStore = db.createObjectStore(
          'list', {keyPath: 'id', autoIncrement: true}
        );
        objStore.createIndex('name_idx', 'name', {unique: false});

      })

      .upgradeDatabase(2, function (event, db) {
        var objStore = db.createObjectStore(
          'book', {keyPath: 'id', autoIncrement: true}
        );
        objStore.createIndex('list_idx', 'listId', {unique: false, multiEntry: true});
        objStore.createIndex('olid_idx', 'olid', {unique: true});
        objStore.createIndex('title_idx', 'title', {unique: false});
        objStore.createIndex('subtitle_idx', 'subtitle', {unique: false});
        objStore.createIndex('cover_idx', 'cover', {unique: false, multiEntry: true});
        objStore.createIndex('author_idx', 'author', {unique: false, multiEntry: true});
        objStore.createIndex('number_of_pages_idx', 'number_of_pages', {unique: false});
        objStore.createIndex('publish_date_idx', 'publish_date', {unique: false});
        objStore.createIndex('revision_idx', 'revision', {unique: false});
        objStore.createIndex('physical_format_idx', 'physical_format', {unique: false});
        objStore.createIndex('physical_dimensions_idx', 'physical_dimensions', {unique: false});
        objStore.createIndex('weight_idx', 'weight', {unique: false});
      });
  }]);
