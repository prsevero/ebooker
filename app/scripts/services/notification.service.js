'use strict';

/**
 * @ngdoc function
 * @name ebookerApp.services:NotificationService
 * @description
 * # NotificationService
 * Service for the Notification
 */
angular.module('ebookerApp')
  .service('NotificationService', ['$rootScope', '$http', '$timeout', function ($rootScope, $http, $timeout) {
    var nServ = {},
        notificationElem;

    $rootScope.notification = {};

    // Bind close event
    notificationElem = document.getElementById('notification');
    notificationElem.addEventListener('click', function (e) {
      if (e.target) {
        if (e.target.matches('a.close') || e.target.matches('a.cancel')) {
          nServ.close();
          e.preventDefault();
        } else if (e.target.matches('a.confirm')) {
          $rootScope.notification.action.fn($rootScope.notification.action.id);
          e.preventDefault();
        }
      }
    });
    // /Bind close event

    nServ.reset = function () {
      $rootScope.$apply(function () {
        $rootScope.notification = {};
      });
    };

    nServ.close = function () {
      $rootScope.$apply(function () {
        if ($rootScope.notification.css_class !== undefined) {
          $rootScope.notification.css_class = $rootScope.notification.css_class.replace('active', '');
        }
      });

      $timeout(function () {
        nServ.reset();
      }, 400);
    };

    nServ.notify = function (css_class, msg, action) {
      $rootScope.notification = {
        css_class: 'active ' + css_class,
        message: msg,
        action: action
      };
    };

    nServ.warning = function (msg, action) {
      nServ.notify('warning', msg, action);
    };

    nServ.success = function (msg) {
      nServ.notify('success', msg);
    };

    $rootScope.$on('$routeChangeStart', function () {
      $timeout(function () {
        nServ.close();
      }, 100);
    });

    return nServ;

  }]);
