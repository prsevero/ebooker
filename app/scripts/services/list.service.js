'use strict';

/**
 * @ngdoc function
 * @name ebookerApp.services:ListService
 * @description
 * # ListService
 * Service for the List
 */
angular.module('ebookerApp')
  .service('ListService', function ($q, $indexedDB) {
    var lServ = {};

    lServ.create = function (list) {
      var deferred = $q.defer();

      $indexedDB.openStore('list', function (store) {
        store.insert(list).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    lServ.delete = function (id) {
      var deferred = $q.defer();

      $indexedDB.openStore('list', function (store) {
        store.delete(parseInt(id)).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    lServ.find = function (id) {
      var deferred = $q.defer();

      $indexedDB.openStore('list', function (lists) {
        lists.find(parseInt(id)).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    lServ.getAll = function () {
      var deferred = $q.defer();

      $indexedDB.openStore('list', function (store) {
        store.getAll().then(function (lists) {
          deferred.resolve(lists);
        });
      });

      return deferred.promise;
    };

    return lServ;

  });
