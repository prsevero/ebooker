'use strict';

/**
 * @ngdoc function
 * @name ebookerApp.services:BookService
 * @description
 * # BookService
 * Service for the Book
 */
angular.module('ebookerApp')
  .service('BookService', function ($q, $indexedDB) {
    var bServ = {};

    bServ.create = function (book) {
      var deferred = $q.defer();

      $indexedDB.openStore('book', function (store) {
        store.insert(book).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    bServ.update = function (params) {
      var deferred = $q.defer();

      $indexedDB.openStore('book', function (store) {
        store.upsert(params).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    bServ.delete = function (id) {
      var deferred = $q.defer();

      $indexedDB.openStore('book', function (store) {
        store.delete(parseInt(id)).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    bServ.find = function (id) {
      var deferred = $q.defer();

      $indexedDB.openStore('book', function (books) {
        books.find(parseInt(id)).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    bServ.findBy = function (index, key) {
      var deferred = $q.defer();

      $indexedDB.openStore('book', function (books) {
        books.findBy(index, key).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    bServ.findAllBy = function (index, key) {
      var deferred = $q.defer();

      $indexedDB.openStore('book', function (books) {
        var query = books.query();
        query.$index(index);
        query.$eq(key);

        books.eachWhere(query).then(function (e) {
          deferred.resolve(e);
        });
      });

      return deferred.promise;
    };

    bServ.findAllUncategorized = function () {
      var deferred = $q.defer();

      bServ.getAll().then(function (books) {
        books = books.filter(function (el) { return el.listId === undefined || !el.listId.length; });
        deferred.resolve(books);
      });

      return deferred.promise;
    };

    bServ.getAll = function () {
      var deferred = $q.defer();

      $indexedDB.openStore('book', function (store) {
        store.getAll().then(function (books) {
          deferred.resolve(books);
        });
      });

      return deferred.promise;
    };

    return bServ;

  });
