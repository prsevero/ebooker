'use strict';

/**
 * @ngdoc function
 * @name ebookerApp.services:OpenLibraryApi
 * @description
 * # OpenLibraryApi
 * Service for the Open Library API
 */
angular.module('ebookerApp')
  .factory('OpenLibraryApi', function ($http, $q) {
    var olApi = {};

    olApi.cancelRequest = function (deferred, reason) {
        deferred.resolve(reason);
    };

    olApi.search = function (name) {
      var deferred = $q.defer();

      var promise = $http.get(
        'https://openlibrary.org/search.json',
        {params: {q: name}, timeout: deferred.promise}
      ).then(function (response) {
        return response.data;
      });

      return {
        promise: promise,
        deferred: deferred
      };
    };

    olApi.getBook = function (olId) {
      var deferred = $q.defer();

      var promise = $http.get(
        'https://openlibrary.org/books/' + olId + '.json',
        {timeout: deferred.promise}
      ).then(function (response) {
        return response.data;
      });

      return {
        promise: promise,
        deferred: deferred
      };
    };

    olApi.getAuthor = function (olId) {
      var deferred = $q.defer();

      var promise = $http.get(
        'https://openlibrary.org/authors/' + olId + '.json',
        {timeout: deferred.promise}
      ).then(function (response) {
        return response.data;
      });

      return {
        promise: promise,
        deferred: deferred
      };
    };
 
    return olApi;
  });
