'use strict';

/**
 * @ngdoc function
 * @name ebookerApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Search Controller of the ebookerApp
 */
angular.module('ebookerApp')
  .controller('SearchCtrl', function ($scope, OpenLibraryApi) {
    var requests = [];

    $scope.search = {
      element: document.getElementById('search-results'),
      timer: undefined,
      displayTimer: undefined,
      name: '',
      searching: false,
      found: [],
      num_found: 0,
      num_showing: 0,
      limit: 10
    };

    $scope.submit = function (e) {
      e.preventDefault();
      return false;
    };

    $scope.searchReset = function () {
      $scope.search.searching = false;
      $scope.search.found = [];
      $scope.search.num_found = 0;
      $scope.search.num_showing = 0;
      $scope.search.limit = 10;
    };

    $scope.searchResultsShow = function () {
      $scope.search.element.style.display = 'block';
    };

    $scope.searchResultsHide = function () {
      $scope.displayTimer = setTimeout(function () {
        $scope.search.element.style.display = 'none';
      }, 300);
    };

    $scope.searchResultsAll = function () {
      clearTimeout($scope.displayTimer);
      $scope.search.limit = $scope.search.num_found;
      $scope.search.num_showing = $scope.search.num_found;
    };

    $scope.searchChange = function () {
      var request;
      if (requests.length) {
        OpenLibraryApi.cancelRequest(requests[0].deferred, 'Search cancelled by user.');
        requests = [];
      }

      $scope.searchReset();
      clearTimeout($scope.search.timer);

      if ($scope.search.name.length > 3) {
        $scope.search.searching = true;
        $scope.search.timer = setTimeout(function () {
          request = OpenLibraryApi.search($scope.search.name);
          requests.push(request);

          request.promise.then(function (response) {
            requests = [];
            $scope.search.searching = false;
            $scope.search.found = response.docs;
            $scope.search.num_found = response.num_found;

            if (response.num_found > 10) {
              $scope.search.num_showing = 10;
            } else {
              $scope.search.num_showing = response.num_found;
            }
          });

        }, 800);
      }
    };

  });
