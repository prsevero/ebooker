'use strict';

/**
 * @ngdoc function
 * @name ebookerApp.controller:ListCtrl
 * @description
 * # ListCtrl
 * List Controller of the ebookerApp
 */
angular.module('ebookerApp')
  .controller(
    'ListCtrl',
    ['$scope', '$route', '$routeParams', '$location', 'ListService', 'BookService', 'NotificationService',
    function ($scope, $route, $routeParams, $location, ListService, BookService, NotificationService) {
      var formCreate = document.getElementById('form-list-create');
      $scope.list = {};
      $scope.lists = [];
      $scope.listToDelete = undefined;
      $scope.listBooks = [];

      $scope.reset = function () {
          $scope.list = {name: ''};
          ListService.getAll().then(function (lists) {
            $scope.lists = lists;
          });
      };

      // Create functions
      $scope.listCreateOpen = function () {
        formCreate.className += ' active';
        formCreate.firstElementChild.focus();
      };

      $scope.listCreate = function (e, addBook) {
        if ($scope.list.name === '') {
          formCreate.firstElementChild.className = formCreate.firstElementChild.className + ' has-error';

        } else {
          formCreate.firstElementChild.className = formCreate.firstElementChild.className.replace(' has-error', '');

          ListService.create($scope.list).then(function (e) {
            if (addBook) {
              ListService.find(e[0]).then(function (list) {
                $scope.addBookToList(list);
              });
            }
          });
          $scope.reset();

          NotificationService.success('List added successfully.');
          formCreate.className = formCreate.className.replace(' active', '');
        }

        e.preventDefault();
      };
      // /Create


      // View functions
      $scope.listView = function (id) {
        if (id === undefined) {
          id = $routeParams.id;
        }

        if (id !== undefined) {
          ListService.find(id).then(function (list) {
            $scope.list = list;

            BookService.findAllBy('list_idx', list.id).then(function (books) {
              $scope.listBooks = books;
            });
          });
        } else {
          BookService.findAllUncategorized().then(function (books) {
            $scope.listBooks = books;
          });
        }
      };
      // /View functions


      // List functions
      $scope.listAll = function () {
        ListService.getAll().then(function (lists) {
          $scope.lists = lists;
        });
      };
      // /List functions


      // Delete functions
      $scope.listDelete = function (id) {
        var is_ajax = true;
        if (id === undefined) {
          id = $routeParams.id;
          is_ajax = false;
        }

        ListService.find(id).then(function (list) {
          $scope.listToDelete = list;

          if (is_ajax) {
            NotificationService.warning(
              'Are you sure to remove list ' + list.name + '?',
              {fn: $scope.listDeleteConfirm, id: id}
            );
          }
        });
      };

      $scope.listDeleteConfirm = function (id) {
        var is_ajax = true;
        if (id === undefined) {
          id = $routeParams.id;
          is_ajax = false;
        }

        // Find all books related to the list
        BookService.findAllBy('list_idx', id).then(function (books) {
          var index;

          // "Uncategorize" them
          for (var i=0, max=books.length; i<max; i++) {
            index = books[i].listId.indexOf(id);

            if (index >= 0) {
              books[i].listId.splice(index, 1);
              BookService.update(books[i]);
            }
          }

          ListService.delete(id).then(function () {
            $scope.lists = $scope.lists.filter(function (elem) {
              return elem.id !== id;
            });

            NotificationService.success('List deleted successfully.');

            if (!is_ajax) {
              $location.url('/');
            }
          });
        });
      };
      // /Delete


      // Book Remove
      $scope.listBookDeleteFromList = function (list, book) {
        NotificationService.warning(
          'Are you sure to remove ' + book.title +' from list ' + list.name + '?',
          {fn: $scope.listBookDeleteFromListConfirm, id: book.id}
        );
      };


      $scope.listBookDeleteFromListConfirm = function (id) {
        BookService.find(id).then(function (book) {
          var index = book.listId.indexOf($scope.list.id);

          if (index >= 0) {
            book.listId.splice(index, 1);
            BookService.update(book).then(function () {
              NotificationService.success('Book removed from the list!');
            });

            BookService.findAllBy('list_idx', $scope.list.id).then(function (books) {
              $scope.listBooks = books;
            });
          } else {
            NotificationService.warning('This book is not in the list.');
          }
        });
      };


      $scope.listBookDelete = function (book) {
        NotificationService.warning(
          'Are you sure to remove book ' + book.title + '?',
          {fn: $scope.listBookDeleteConfirm, id: book.id}
        );
      };

      $scope.listBookDeleteConfirm = function (id) {
        BookService.delete(id).then(function () {
          $scope.listView($scope.list.id);

          NotificationService.success('Book deleted successfully.');
        });
      };
      // /Book Remove

      function init () {
        $scope.reset();
        if ($location.path().indexOf('list') >= 0 && $route.current.method !== undefined) {
          $scope[$route.current.method]();
        }
      }
      init();

    }]
  );
