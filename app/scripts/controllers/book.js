'use strict';

/**
 * @ngdoc function
 * @name ebookerApp.controller:BookCtrl
 * @description
 * # BookCtrl
 * Book Controller of the ebookerApp
 */
angular.module('ebookerApp')
  .controller(
    'BookCtrl',
    ['$scope', '$route', '$routeParams', '$location', 'OpenLibraryApi', 'BookService', 'NotificationService',
    function ($scope, $route, $routeParams, $location, OpenLibraryApi, BookService, NotificationService) {
      $scope.book = {};

      $scope.reset = function () {
        $scope.book = undefined;

        if ($routeParams.id !== undefined) {
          BookService.findBy('olid_idx', $routeParams.id).then(function (book) {
            $scope.book = book;
          });
        }
      };

      // Create functions
      $scope.bookCreate = function (book) {
        BookService.create({
          'olid': book.olid,
          'title': book.title,
          'subtitle': book.subtitle,
          'cover': book.covers,
          'author': book.authors_data,
          'number_of_pages': book.number_of_pages,
          'publish_date': book.publish_date,
          'revision': book.revision,
          'physical_format': book.physical_format,
          'physical_dimensions': book.physical_dimensions,
          'weight': book.weight
        }).then(function (e) {
          BookService.find(e[0]).then(function (book) {
            $scope.book = book;
            $scope.book.added = true;
            $scope.book.covers = $scope.book.cover;
            $scope.book.authors_data = $scope.book.author;
          });
        });

        NotificationService.success('Book added successfully.');
      };
      // /Create


      // $scope.bookLists


      $scope.addBookToList = function (list) {
        if ($scope.book.listId === undefined) {
          $scope.book.listId = [];
        }
        $scope.book.listId.push(list.id);

        BookService.update($scope.book).then(function () {
          NotificationService.success('Book added to list!');
        });
      };


      $scope.removeBookFromList = function (list) {
        var index = $scope.book.listId.indexOf(list.id);

        if (index >= 0) {
          $scope.book.listId.splice(index, 1);
          BookService.update($scope.book).then(function () {
            NotificationService.success('Book removed from the list!');
          });
        } else {
          NotificationService.warning('This book is not in the list.');
        }
      };


      // View functions
      $scope.getAuthor = function (author) {
        OpenLibraryApi.getAuthor(author.key.replace('/authors/', '')).promise.then(function (response) {
          $scope.book.authors_data.push(response);
        });
      };


      $scope.bookView = function (id) {
        if (id === undefined) {
          id = $routeParams.id;
        }

        // Try to recover book locally first
        BookService.findBy('olid_idx', id).then(function (book) {
          if (book) {
            $scope.book = book;
            $scope.book.added = true;
            $scope.book.covers = $scope.book.cover;
            $scope.book.authors_data = $scope.book.author;
          } else {
            OpenLibraryApi.getBook(id).promise.then(function (response) {
              $scope.book = response;
              $scope.book.added = false;
              $scope.book.olid = id;
              $scope.book.authors_data = [];

              if (response.authors !== undefined) {
                for (var i=0, max=response.authors.length; i<max; i++) {
                  $scope.getAuthor(response.authors[i]);
                }
              }
            });
          }
        });
      };
      // /View functions


      // Delete functions
      $scope.bookDelete = function (id) {
        NotificationService.warning(
          'Are you sure to remove book ' + $scope.book.title + '?',
          {fn: $scope.bookDeleteConfirm, id: id}
        );
      };

      $scope.bookDeleteConfirm = function (id) {
        BookService.delete(id).then(function () {
          $scope.book.added = false;

          NotificationService.success('Book deleted successfully.');
        });
      };
      // /Delete

      function init () {
        if ($location.path().indexOf('book') >= 0 && $route.current.method !== undefined) {
          $scope.reset();
          $scope[$route.current.method]();
        }
      }
      init();

    }]
  );
